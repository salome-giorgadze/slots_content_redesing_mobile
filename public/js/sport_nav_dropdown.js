const sportLi = document.querySelectorAll(".sp_item_link");
const subLi = document.querySelectorAll(".sportSubList_item_link");
const checkedItems = document.querySelectorAll(".sportSubscheckList_link")

function showDropdownMenu(arr) {
    for (let item of arr) {
        item.addEventListener("click", function () {
            item.parentNode.classList.toggle("show_subList");
        });
    }
}
showDropdownMenu(sportLi);
showDropdownMenu(subLi)

function checkedItem(arr) {
    for (let item of arr) {
        item.addEventListener("click", function () {
            item.classList.toggle("checked");
        });
    }
}

checkedItem(checkedItems)

